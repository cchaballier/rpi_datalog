#!/usr/bin/python
import time
import smbus
import logging
import logging.handlers
from logging import Formatter
from periodic import Periodic

LOG_FILENAME = '/mnt/storage/Log_test.log'
MEASURE_PERIOD_SECONDS = 2

bus = smbus.SMBus(1)
my_logger = logging.getLogger('MyLogger')

global current_temperature
global current_humidity
current_temperature = 0
current_humidity = 0

class CellarMonitor(object):
    def __init__(self):
        bus.read_i2c_block_data(0x27,0)

        # around 1.4MB of data per 24h/day with 2 seconds periodic samples. Stores up to 40 days.
        handler = logging.handlers.RotatingFileHandler(LOG_FILENAME,maxBytes=1500000, backupCount=40)
        FORMAT = '%(asctime)s,%(message)s'
        DATE_FORMAT = '%m/%d/%Y %H:%M:%S'
        formatter = Formatter(fmt=FORMAT, datefmt=DATE_FORMAT)
        handler.setFormatter(formatter)

        my_logger.setLevel(logging.INFO)
        my_logger.addHandler(handler)

    def start_monitoring(self):
        rt = Periodic(MEASURE_PERIOD_SECONDS, self.DoMeasure) # it auto-starts, no need of rt.start()

    def DoMeasure(self):
        global current_temperature
        global current_humidity
        data = bus.read_i2c_block_data(0x27,0)
        current_humidity = (((data[0] & 0x3F)*256) + (data[1]))/16382.0 * 100
#        print "H: %.2f %% " % humidity
        current_temperature = (((((data[3] & 0xFC)>>2)+ (data[2]*64))/16382.0) * 165.0) - 40.0
 #       print "T: %.2f degC" % temperature
        logString = str("%.2f" % current_temperature) + "," + str("%.2f" % current_humidity)
        my_logger.info(logString)

    def GetCurrentTemperature(self):
        return current_temperature

    def GetCurrentHumidity(self):
        return current_humidity
	
