#!/usr/bin/env python
import sys 
if sys.version_info[0] != 2 or sys.version_info[1] <= 6:
    print("Lcd_controller only works with `python 2.7.x`.")
    sys.exit(1)

import os
import time
from cellar_monitor import CellarMonitor

import pifacecommon
import pifacecad

degree_pix = pifacecad.LCDBitmap([0b00111, 
				0b00101,
				0b00111,
				0b00000,
				0b00000,
				0b00000,
				0b00000,
				0b00000])

global monitor

global screen_page
SCREEN_PAGE_MONITORING = 0
SCREEN_PAGE_CONTROLLER = 1
screen_page = SCREEN_PAGE_CONTROLLER

TARGET_MAX_TEMPERATURE = 25
TARGET_MIN_TEMPERATURE = 6
TARGET_DEFAULT_TEMPERATURE = 13

global current_temperature
global current_temperature_status
current_temperature = TARGET_DEFAULT_TEMPERATURE
current_temperature_status = "OK"
global cad

class Screen(object):
	def __init__(self, cad):
		cad.lcd.store_custom_bitmap(0,degree_pix)
		cad.lcd.backlight_on()
		cad.lcd.cursor_off()
		cad.lcd.blink_off()

		cad.lcd.write(" - Welcome - ");
		cad.lcd.set_cursor(0,1)
		cad.lcd.cursor_off()
		cad.lcd.write("RPI datalogger")
		time.sleep(3)
		cad.lcd.clear()
                global screen_page
                screen_page = SCREEN_PAGE_MONITORING 

        def init_page(self):
            if screen_page == SCREEN_PAGE_CONTROLLER:
                cad.lcd.clear()
                controller.DisplayTemperature(cad)
                controller.DisplayHygro(cad)
            else: 
                cad.lcd.clear()
                cad.lcd.write(" Temp.   Humid. ");

        def updateMonitorPage(self, temp, hum):
                cad.lcd.set_cursor(0,1)
                logstr = str("%.2f" % temp)+ " C  " + str("%.2f" % hum)+ " %"
                cad.lcd.write(logstr);




class RegulationController(object):
	def updateTemperature(self, increment_bool):
		global current_temperature
		if increment_bool == True:
			if current_temperature < TARGET_MAX_TEMPERATURE:
				current_temperature += 1
				current_temperature_status = "OK"
			#else:
			#	current_temperature_status = "MAX"
				
		else:
			if current_temperature > TARGET_MIN_TEMPERATURE:
				current_temperature -= 1
				current_temperature_status = "OK"
			#else:
			#	current_temperature_status = "MIN"

	def DisplayTemperature(self, cad):
		cad.lcd.home()
		cad.lcd.write("Temp: ")
		cad.lcd.write(str(current_temperature))
		cad.lcd.write_custom_bitmap(0)#degree_pix
		cad.lcd.write("C ")
		self.DisplayTemperatureStatus(current_temperature_status)

	def DisplayTemperatureStatus(self,status):
		cad.lcd.set_cursor(12,0)
		cad.lcd.cursor_off()
		cad.lcd.write(status)

	def DisplayHygro(self, cad):
		cad.lcd.set_cursor(0,1)
		cad.lcd.cursor_off()
		cad.lcd.write("Hygro: 70%  OK ")


def update_pin(event):
    global controller
    global screen_page
    if event.pin_num == 4:
        if screen_page == SCREEN_PAGE_CONTROLLER:
            controller.updateTemperature(True)
            controller.DisplayTemperature(cad)
	elif event.pin_num == 3:
            if screen_page == SCREEN_PAGE_CONTROLLER:
                controller.updateTemperature(False)
                controller.DisplayTemperature(cad)
        elif event.pin_num == 2:
            if screen_page == SCREEN_PAGE_CONTROLLER:
                screen_page = SCREEN_PAGE_MONITORING
                screen.init_page()
            elif screen_page == SCREEN_PAGE_MONITORING:
                screen_page = SCREEN_PAGE_CONTROLLER
                screen.init_page()


if __name__ == "__main__":
	
	# instanciate objects
	cad = pifacecad.PiFaceCAD()
	global screen 
	screen = Screen(cad)
	global controller
	controller = RegulationController()

	# register events on inputs pins
#	listener = pifacecad.SwitchEventListener(chip=cad)
#   listener.register(2,pifacecad.IODIR_FALLING_EDGE, update_pin)
#	listener.register(3,pifacecad.IODIR_FALLING_EDGE, update_pin)
#	listener.register(4,pifacecad.IODIR_FALLING_EDGE, update_pin)
#	listener.activate()

	# initialize screen data	
    screen.init_page()

    # initialize background monitoring
    monitor = CellarMonitor()
    monitor.start_monitoring()

	# wait forever
	while True:
		time.sleep(2)
                if screen_page == SCREEN_PAGE_MONITORING:
                    screen.updateMonitorPage( monitor.GetCurrentTemperature(), monitor.GetCurrentHumidity())



