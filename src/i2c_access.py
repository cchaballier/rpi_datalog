#!/usr/bin/python
import time
import smbus
import logging
import logging.handlers
from logging import Formatter
from periodic import Periodic


LOG_FILENAME = '/mnt/storage/Log_test.log'
MEASURE_PERIOD_SECONDS = 5

bus = smbus.SMBus(1)
#bus.write_quick()
bus.read_i2c_block_data(0x27,0)

handler = logging.handlers.RotatingFileHandler(LOG_FILENAME,maxBytes=100, backupCount=5)

FORMAT = '%(asctime)s,%(message)s'
DATE_FORMAT = '%m/%d/%Y %I:%M:%S'
formatter = Formatter(fmt=FORMAT, datefmt=DATE_FORMAT)
handler.setFormatter(formatter)

my_logger = logging.getLogger('MyLogger')
my_logger.setLevel(logging.INFO)
my_logger.addHandler(handler)

def DoMeasure():
    data = bus.read_i2c_block_data(0x27,0)
    humidity = (((data[0] & 0x3F)*256) + (data[1]))/16382.0 * 100
    print "H: %.2f %% " % humidity
    temperature = (((((data[3] & 0xFC)>>2)+ (data[2]*64))/16382.0) * 165.0) - 40.0
    print "T: %.2f degC" % temperature
    logString = str("%.2f" % temperature) + "," + str("%.2f" % humidity)
    my_logger.info(logString)
	
rt = Periodic(MEASURE_PERIOD_SECONDS, DoMeasure) # it auto-starts, no need of rt.start()

while 1:
    time.sleep(1)
